from django.db import models
from django.urls import reverse
from django.conf import settings

# Modelli per la Board, per le colonne che appartengono alla board e per le card appartenenti alle colonne


# Board
class Board(models.Model):

    name = models.CharField(max_length=25)
    admin = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete='CASCADE', related_name='admin')
    associates = models.ManyToManyField(settings.AUTH_USER_MODEL)

    def __str__(self):
        return self.name.__str__()

    def __unicode__(self):
        return self.name.__str__()

    def get_absolute_url(self):

        return reverse(viewname='board_detail', args=[str(self.id)])

    class Meta:

        constraints = [models.UniqueConstraint(fields=['name', 'admin'], name='admins_board')]

# Colonna
class Column(models.Model):

    name = models.CharField(max_length=25)
    of_board = models.ForeignKey('Board', on_delete=models.CASCADE)

    def __str__(self):
        return self.name.__str__()

    def __unicode__(self):
        return self.name.__str__()

    def get_absolute_url(self):

        return reverse(viewname='modify_column', args=[str(self.id)])

    class Meta:

        constraints = [models.UniqueConstraint(fields=['name', 'of_board'], name='boards_column')]


#Card
class Card(models.Model):

    title = models.CharField(max_length=25)
    description = models.CharField(max_length=80)

    associates = models.ManyToManyField(settings.AUTH_USER_MODEL)

    of_board = models.ForeignKey('Board', on_delete=models.CASCADE)
    of_column = models.ForeignKey('Column', on_delete=models.CASCADE)

    date_creation = models.DateField()
    expiration_date = models.DateField()

    story_points = models.SmallIntegerField()

    def __str__(self):
        return self.title.__str__() + " [" + self.expiration_date.__str__() + "]"

    def __unicode__(self):
        return self.title.__str__() + " [" + self.expiration_date.__str__() + "]"

    def get_absolute_url(self):

        return reverse(viewname='modify_card', args=[str(self.id)])

    class Meta:
        constraints = [models.UniqueConstraint(fields=['title', 'of_board'], name='boards_card')]
