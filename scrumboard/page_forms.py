from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

# Form per la registrazione di un utente
class SignUpForm(UserCreationForm):

    first_name = forms.CharField(max_length=25, required=True)
    last_name = forms.CharField(max_length=25, required=True)
    email = forms.EmailField(max_length=254)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', )

# Form per il login di un utente già registrato
class LoginForm(forms.Form):

    username = forms.CharField(label='Username', max_length=25)
    password = forms.CharField(label='Password', max_length=25, widget=forms.PasswordInput)

