from django.shortcuts import render
from scrumboard import page_forms
from scrumboard.page_forms import SignUpForm
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.contrib import messages

# Questa view gestisce la registrazione all'applicazione web

def signup(request):
    # Se il form viene compilato e inviato col metodo POST
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect(to='dashboard')

        else:  # Errore: Form non completamente compilato o dati scorretti

            messages.error(request, "I dati inseriti non sono corretti o mancanti! Ricontrolla i dati inseriti.")
            return render(request, 'sign_up.html', {'form': page_forms.SignUpForm()})

    else:  # Accesso alla pagina con metodo GET

        return render(request, 'sign_up.html', {'form': page_forms.SignUpForm()})

